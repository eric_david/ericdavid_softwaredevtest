﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly decimal amount;
        public readonly string transactionType;

        public const string DEPOSIT = "deposit";
        public const string WITHDRAWAL = "withdrawal";
        public const string INTEREST = "interest";

        public DateTime transactionDate { get; private set; }

        public Transaction(decimal amount, string type)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
            this.transactionType = type;
        }
    }
}
