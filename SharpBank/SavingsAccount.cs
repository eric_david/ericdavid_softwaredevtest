﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class SavingsAccount : Account
    {
        private const int type = SAVINGS;

        public SavingsAccount() : base(type)
        {
        }

        public override decimal InterestEarned()
        {
            decimal amount = this.SumTransactions();

            if (amount <= 1000)
                return CalculateInterest(amount, interestRates[Account.savingsLowBalanceRate]);
            else
                return 1 + CalculateInterest((amount - 1000), interestRates[Account.savingsLowBalanceRate]);
        }

        public override decimal CalculateInterest(decimal amount, decimal rate)
        {
            return base.CalculateInterest(amount, rate);
        }
    }
}
