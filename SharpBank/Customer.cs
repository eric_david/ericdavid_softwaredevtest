﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        public const string _invalidNameArgumentMessage = "Customer name must be a string of at least 1 character";
        public const string _invalidAccountsMessage = "Both accounts must be valid for transfer";

        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentException(_invalidNameArgumentMessage);

            this.name = name;
            accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public decimal TotalInterestEarned()
        {
            decimal total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        public bool CustomerHasEnoughAccounts()
        {
            if (accounts.Count >= 2)
                return true;

            return false;
        }

        public bool CustomerHasSufficientFundsForTransfer(Account source, decimal amount)
        {
            if (source.SumTransactions() >= amount)
                return true;

            return false;
        }

        public void TransferFunds(Account source, Account target, decimal amount)
        {
            if (source == null || target == null)
                throw new ArgumentException("Both accounts must be valid for transfer");

            if (CustomerHasEnoughAccounts() && CustomerHasSufficientFundsForTransfer(source, amount))
            {
                object obj = new object();
                lock (obj)
                {
                    source.Withdraw(amount);
                    target.Deposit(amount);
                }                
            }
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            decimal total = 0.0m;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            decimal total = 0.0m;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        private String ToDollars(decimal d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
