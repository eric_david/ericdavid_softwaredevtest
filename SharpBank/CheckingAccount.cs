﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class CheckingAccount : Account
    {
        private const int type = CHECKING;

        public CheckingAccount() : base(type)
        {
        }

        public override decimal InterestEarned()
        {
            return SumTransactions(r => r.transactionType == Transaction.DEPOSIT || r.transactionType == Transaction.WITHDRAWAL) * this.interestRates[Account.defaultCheckingRate];
        }
    }
}
