﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavingsAccount : Account
    {
        private const int type = MAXI_SAVINGS;

        public MaxiSavingsAccount() : base(type)
        {
        }

        public override decimal InterestEarned()
        {
            decimal amount = SumTransactions();

            var lastWithdrawalTransaction = transactions.LastOrDefault(p => p.transactionType == Transaction.WITHDRAWAL);
            TimeSpan lastWithdrawalSpan = TimeSpan.MinValue;

            if (lastWithdrawalTransaction != null)
            {
                lastWithdrawalSpan = DateTime.Now.Date - lastWithdrawalTransaction.transactionDate;
            }
            else
            {
                lastWithdrawalSpan = DateTime.Now.Date - transactions.First().transactionDate.Date;
            }

            if (lastWithdrawalSpan.Days >= 10)
            {
                return CalculateInterest(amount, interestRates[Account.maxiHighRate]);
            }
            else
                return CalculateInterest(amount, interestRates[Account.maxiLowRate]);
        }
    }
}
