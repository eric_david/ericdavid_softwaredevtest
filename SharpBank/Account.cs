﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {
        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        public const string amountMustBeGreaterThanZero = "Amount must be greater than zero";
        public const string insufficientFunds = "Insufficient Funds";

        public const string defaultCheckingRate = "defaultChecking";
        public const string maxiHighRate = "maxiHighRate";
        public const string maxiLowRate = "maxiLowRate";
        public const string savingsLowBalanceRate = "savingsLowBalanceRate";
        public const string savingsHigherBalanceRate = "savingsHigherBalanceRate";

        private readonly int accountType;
        public List<Transaction> transactions;
        protected Dictionary<string, decimal> interestRates = new Dictionary<string, decimal>();

        public Dictionary<string, decimal> InterestRates {
            get
            {
                return interestRates;
            }
        }

        public Account(int accountType)
        {
            this.accountType = accountType;
            transactions = new List<Transaction>();

            interestRates.Add(defaultCheckingRate, .001m / 365m);
            interestRates.Add(maxiHighRate, .05m / 365m);
            interestRates.Add(maxiLowRate, .001m / 365m);
            interestRates.Add(savingsLowBalanceRate, .001m / 365m);
            interestRates.Add(savingsHigherBalanceRate, .002m / 365m);
        }

        public void Deposit(decimal amount, string type = Transaction.DEPOSIT)
        {
            if (amount <= 0)
            {
                throw new ArgumentException(amountMustBeGreaterThanZero);
            }
            else
            {
                transactions.Add(new Transaction(amount, type));
            }
        }

        /// <summary>
        /// adding validation to make sure account has sufficient funds
        /// </summary>
        /// <param name="amount"></param>
        public void Withdraw(decimal amount)
        {
            if(amount > SumTransactions())
            {
                throw new InvalidOperationException(insufficientFunds);
            }

            if (amount <= 0)
            {
                throw new ArgumentException(amountMustBeGreaterThanZero);
            }
            else
            {
                transactions.Add(new Transaction(-amount, Transaction.WITHDRAWAL));
            }
        }

        /// <summary>
        /// default interest rate, currently applies to Checking accounts
        /// </summary>
        /// <returns></returns>
        public virtual decimal InterestEarned()
        {
            return CalculateInterest(SumTransactions(), interestRates[defaultCheckingRate]);
        }

        public virtual decimal CalculateInterest(decimal amount, decimal rate)
        {
            return amount * rate;
        }

        public virtual decimal CalculateDailyBalance(Func<Transaction, bool> filter = null)
        {
            return SumTransactions(filter);
        }

        /// <summary>
        /// We need to be able to filter transactions in order to have the flexibility to drill down on types and date ranges
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public decimal SumTransactions(Func<Transaction, bool> filter = null)
        {
            decimal amount = 0.0m;

            foreach (Transaction t in transactions.Where(filter ?? (t => true)))
                amount += t.amount;
            return amount;
        }

        public int GetAccountType()
        {
            return accountType;
        }

        public int GetDateRangeOfTransactions()
        {
            return transactions.Any() ? (transactions.Last().transactionDate.Date - transactions.First().transactionDate.Date).Days : 0;
        }
    }
}
