﻿using System;

namespace SharpBank
{
    public interface ITimeManager
    {
        DateTime Now { get; }
    }

    public class TestTimeManager : ITimeManager
    {
        public Func<DateTime> CurrentTime = () => DateTime.Now;

        public void SetDateTime(DateTime now)
        {
            CurrentTime = () => now;
        }

        public DateTime Now => CurrentTime();
    }

    public class DateProvider
    {
        private static DateProvider instance = null;

        /// <summary>
        /// I'm not really happy about what I've added to this implementation, and I think this deserves further refactoring 
        /// but it appears to work sufficiently well for testing
        /// </summary>
        /// <returns></returns>
        private readonly ITimeManager _timeManager = null;

        private DateProvider()
        {
        }

        /// <summary>
        /// this should only be used for testing purposes
        /// </summary>
        /// <param name="timeManager"></param>
        public DateProvider(ITimeManager timeManager)
        {
            _timeManager = timeManager;
            instance = this;
        }

        public static DateProvider GetInstance()
        {
            if (instance == null)
                instance = new DateProvider();
            return instance;
        }

        /// <summary>
        /// only for testing purposes
        /// </summary>
        /// <returns></returns>
        public static DateProvider ResetInstance()
        {
            instance = new DateProvider();
            return instance;
        }

        public DateTime Now()
        {
            return _timeManager == null ? DateTime.Now : _timeManager.Now;
        }
    }
}
