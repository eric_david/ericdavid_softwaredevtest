﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly decimal DECIMAL_DELTA = 1e-15m;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new SavingsAccount());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());

            Customer bob = new Customer("Bob");
            bob.OpenAccount(new CheckingAccount());
            bank.AddCustomer(bob);

            Assert.AreEqual("Customer Summary\n - John (1 account)\n - Bob (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void AddCustomer()
        {
            Bank bank = new Bank();
            Customer bill = new Customer("Bill");
            bank.AddCustomer(bill);

            var customerSummary = bank.CustomerSummary();
            Assert.AreEqual("Customer Summary\n - Bill (0 accounts)", customerSummary, "Customer summary does not match");
        }

        [Test]
        public void TestTotalInterestPaid()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            var savingsAccount = new SavingsAccount();

            Customer bill = new Customer("Bill").OpenAccount(checkingAccount).OpenAccount(savingsAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0m);
            savingsAccount.Deposit(2000);

            Assert.AreEqual(1.0030m, Math.Round(bank.TotalInterestPaid(), 4));
        }
    }
}
