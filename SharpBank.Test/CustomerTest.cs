﻿using System;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        private static readonly decimal DECIMAL_DELTA = 1e-15m;

        /// <summary>
        /// We want to make sure that when a customer is created it has a name
        /// </summary>
        [Test]
        public void TestExceptionIfCustomerNameIsNullOrEmpty()
        {
            Assert.Throws<ArgumentException>(() => new Customer(null), Customer._invalidNameArgumentMessage);
        }

        [Test]
        public void TestCustomerCreationWithName()
        {
            var customer = new Customer("Susan");
            Assert.AreEqual(customer.GetName(), "Susan");
        }

        [Test]
        public void TestCustomerStatementGeneration()
        {

            var checkingAccount = new CheckingAccount();
            var savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0m);
            savingsAccount.Deposit(4000.0m);
            savingsAccount.Withdraw(200.0m);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestNoAccounts()
        {
            Customer oscar = new Customer("Oscar");
            Assert.AreEqual(0, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestSomeNumberOfAccountsExist()
        {
            Customer oscar = new Customer("Oscar");
            var randomLength = new Random();
            int accountLength = randomLength.Next(1, 6);

            for (int i = 0; i < accountLength; i++)
            {
                oscar.OpenAccount(new SavingsAccount());
            }

            Assert.AreEqual(accountLength, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestCustomerDoesNotHaveEnoughAccounts()
        {
            Customer bill = new Customer("Bill");
            var savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(500);
            Assert.IsFalse(bill.CustomerHasEnoughAccounts());
        }

        [Test]
        public void TestCustomerDoesHaveEnoughAccounts()
        {
            Customer bill = new Customer("Bill");
            var savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(500);
            bill.OpenAccount(savingsAccount);

            var checking = new CheckingAccount();
            bill.OpenAccount(checking);

            Assert.IsTrue(bill.CustomerHasEnoughAccounts());
        }

        [Test]
        public void TestThatCustomerCannotTransferWhenInsufficientFunds()
        {
            Customer bill = new Customer("Bill");
            var savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(1);
            savingsAccount.Withdraw(1);
            bill.OpenAccount(savingsAccount);

            var checking = new CheckingAccount();
            bill.OpenAccount(checking);

            Assert.IsFalse(bill.CustomerHasSufficientFundsForTransfer(savingsAccount, 1));
        }

        [Test]
        public void TestTransferBetweenAccountsWithout2Accounts()
        {
            Customer bill = new Customer("Bill");
            var savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(500);
            Assert.Throws<ArgumentException>(() => bill.TransferFunds(savingsAccount, null, 100), Customer._invalidAccountsMessage);
        }

        [Test]
        public void TestTransferBetweenAccounts()
        {
            Customer bill = new Customer("Bill");
            var savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(1000);
            bill.OpenAccount(savingsAccount);

            var checking = new CheckingAccount();
            bill.OpenAccount(checking);
            bill.TransferFunds(savingsAccount, checking, 250);

            Assert.AreEqual(750, savingsAccount.SumTransactions(), "Source account balance after transfer is incorrect", null);
            Assert.AreEqual(250, checking.SumTransactions(), "Target account balance after transfer is incorrect", null);
        }
    }
}
