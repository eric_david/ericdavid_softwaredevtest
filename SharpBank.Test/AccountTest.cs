﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        private const string accountIsNull = "Account is null";
        private const string accountTypesDoNotMatch = "Account types do not match";
        private const string dailyBalanceIncorrect = "Daily balance with interest incorrect";

        private static readonly decimal DECIMAL_DELTA = 1e-15m;

        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void TestWithdrawCheckingWhenInsufficientFunds()
        {
            var account = new CheckingAccount();
            Assert.Throws<InvalidOperationException>(() => account.Withdraw(2000), Account.insufficientFunds);
        }

        [Test]
        public void TestWithdrawFromSavingsWhenInsufficientFunds()
        {
            var account = new SavingsAccount();
            Assert.Throws<InvalidOperationException>(() => account.Withdraw(2000), Account.insufficientFunds);
        }

        [Test]
        public void TestWithdrawFromMaxiSavingsWhenInsufficientFunds()
        {
            var account = new MaxiSavingsAccount();
            Assert.Throws<InvalidOperationException>(() => account.Withdraw(2000), Account.insufficientFunds);
        }

        [Test]
        public void TestCreateCheckingAccount()
        {
            var account = new CheckingAccount();
            Assert.IsNotNull(account);
        }

        [Test]
        public void TestCalculateCheckingAccountInterestEarned()
        {
            var account = new CheckingAccount();
            account.Deposit(2000);
            var interest = account.InterestEarned();
            Assert.AreEqual(0.0055m, Math.Round(interest, 4), "Interest calculation is incorrect");
        }

        [Test]
        public void TestCreateSavingsAccount()
        {
            var account = new SavingsAccount();
            Assert.IsNotNull(account);
        }

        [Test]
        public void TestCalculateSavingsAccountInterestEarned()
        {
            var account = new SavingsAccount();
            account.Deposit(2000);
            var interest = account.InterestEarned();
            Assert.AreEqual(1.0027m, Math.Round(interest, 4), "Interest calculation is incorrect");
        }

        [Test]
        public void TestCreateMaxiSavingsAccount()
        {
            var account = new MaxiSavingsAccount();
            Assert.IsNotNull(account);
        }

        [Test]
        public void TestCalculateMaxiSavingsAccountInterestEarned()
        {
            var account = new MaxiSavingsAccount();
            account.Deposit(5500);
            var interest = account.InterestEarned();
            Assert.AreEqual(0.0151m, Math.Round(interest, 4), "Interest calculation is incorrect");
        }

        [Test]
        public void TestCalculateNEWMaxiSavingsAccountInterestEarned()
        {
            var testTime = new TestTimeManager();
            testTime.SetDateTime(new DateTime(2019, 1, 2, 2, 0, 0));
            var dateProvider = new DateProvider(testTime);

            var account = new MaxiSavingsAccount();
            account.Deposit(5500);
            var interest = account.InterestEarned();
            Assert.AreEqual(0.7534m, Math.Round(interest, 4), "Interest calculation is incorrect");
            DateProvider.ResetInstance();
        }

        [Test]
        public void TestGettingFirstTransaction()
        {
            var account = new SavingsAccount();
            account.Deposit(2000);

            var transaction = account.transactions.FirstOrDefault();
            Assert.AreEqual(2000, transaction.amount);
            Assert.AreEqual(DateTime.Now.Date, transaction.transactionDate.Date);
        }

        [Test]
        public void TestGettingLastTransaction()
        {
            var account = new SavingsAccount();
            account.Deposit(2000);
            account.Withdraw(200);

            var transaction = account.transactions.LastOrDefault();
            Assert.AreEqual(-200, transaction.amount);
            Assert.AreEqual(DateTime.Now.Date, transaction.transactionDate.Date);
        }

        [Test]
        public void TestGettingDateRangeFromZeroTransactions()
        {
            var account = new SavingsAccount();
            var daysElapsed = account.GetDateRangeOfTransactions();
            Assert.AreEqual(0, daysElapsed);
        }

        [Test]
        public void TestGettingDateRangeFromTransactions()
        {
            var testTime = new TestTimeManager();
            testTime.SetDateTime(new DateTime(2019, 1, 2, 2, 0, 0));
            var dateProvider = new DateProvider(testTime);

            var account = new SavingsAccount();
            account.Deposit(2000);

            testTime.SetDateTime(new DateTime(2019, 1, 3, 3, 0, 0));
            account.Deposit(1000);

            testTime.SetDateTime(new DateTime(2019, 1, 10, 3, 0, 0));
            account.Deposit(1000);

            var daysElapsed = account.GetDateRangeOfTransactions();
            Assert.AreEqual(8, daysElapsed);

            DateProvider.ResetInstance();
        }

        [Test]
        public void TestCalculateDailyBalance()
        {
            var account = new CheckingAccount();
            account.Deposit(2500);

            var total = account.CalculateDailyBalance();
            Assert.AreEqual(2500.00m, total);
        }

        [Test]
        public void TestTransactionDateSumFilter()
        {
            var testTime = new TestTimeManager();
            var start = new DateTime(2019, 1, 2);
            testTime.SetDateTime(start);
            var dateProvider = new DateProvider(testTime);

            var account = new CheckingAccount();
            account.Deposit(2500);
            account.Withdraw(200);

            var end = new DateTime(2019, 1, 3);
            testTime.SetDateTime(end);

            account.Deposit(2.5m, Transaction.INTEREST);
            account.Deposit(2500m);

            var transForDay1 = account.SumTransactions(r => r.transactionDate.Equals(DateTime.Parse("1/2/2019")));
            Assert.AreEqual(2300, transForDay1);

            var transForDay2 = account.SumTransactions(r => r.transactionDate.Equals(DateTime.Parse("1/3/2019")));
            Assert.AreEqual(2502.5, transForDay2);

            DateProvider.ResetInstance();
        }

        [Test]
        public void TestTransactionTypeSumFilter()
        {
            var account = new CheckingAccount();
            account.Deposit(2500);
            account.Withdraw(200);

            var sum = account.SumTransactions(r => r.transactionType == Transaction.DEPOSIT);
            Assert.AreEqual(2500, sum);

            sum = account.SumTransactions(r => r.transactionType == Transaction.WITHDRAWAL);
            Assert.AreEqual(-200, sum);

            sum = account.SumTransactions(r => r.transactionType == Transaction.INTEREST);
            Assert.AreEqual(0, sum);

            sum = account.SumTransactions();
            Assert.AreEqual(2300, sum);

            account.Deposit(2.5m, Transaction.INTEREST);
            sum = account.SumTransactions(r => r.transactionType == Transaction.INTEREST);
            Assert.AreEqual(2.5m, sum);

            sum = account.SumTransactions();
            Assert.AreEqual(2302.5m, sum);
        }


        // 1. find number of days
        // 2. for each day calculate daily balance
        // 3. calculate the daily interest 
        // 4. add it to the balance
        // 5. record the interest as an interest deposit
        [Test]
        public void TestDailyBalance_Checking_OverRangeOfDays()
        {
            var testTime = new TestTimeManager();
            var start = new DateTime(2019, 1, 2);
            testTime.SetDateTime(start);
            var dateProvider = new DateProvider(testTime);

            var dailyRate = .001m / 365m;
            var account = new CheckingAccount();
            account.Deposit(2500);

            var nextDayWithDeposit = new DateTime(2019, 1, 15);
            testTime.SetDateTime(nextDayWithDeposit);
            account.Deposit(1000);

            var numberOfDays = account.GetDateRangeOfTransactions();
            var balance = 0m;

            var range = Enumerable.Range(0, 1 + account.transactions.LastOrDefault().transactionDate.Subtract(account.transactions.FirstOrDefault().transactionDate).Days)
              .Select(offset => start.AddDays(offset))
              .ToArray();

            for (int i = 0; i < range.Length; i++)
            {                
                balance += account.SumTransactions(t => t.transactionDate.Equals(range[i]) && 
                t.transactionType == Transaction.DEPOSIT || t.transactionType == Transaction.WITHDRAWAL);
                var interest = account.CalculateInterest(balance, dailyRate);
                testTime.SetDateTime(range[i]);
                account.Deposit(interest, Transaction.INTEREST);
                balance += interest;
            }

            Assert.AreEqual(3500.0986, Math.Round(balance, 4));
            DateProvider.ResetInstance();
        }

        //this is not ready for prime time yet...
        //[Test]
        //public void TestDailyBalance_Maxi_OverRangeOfDays()
        //{
        //    var testTime = new TestTimeManager();
        //    var start = new DateTime(2019, 1, 2);
        //    testTime.SetDateTime(start);
        //    var dateProvider = new DateProvider(testTime);

            
        //    var account = new MaxiSavingsAccount();
        //    account.Deposit(2500);

        //    var nextDayWithDeposit = new DateTime(2019, 1, 15);
        //    testTime.SetDateTime(nextDayWithDeposit);
        //    account.Deposit(1000);

        //    var numberOfDays = account.GetDateRangeOfTransactions();
        //    var balance = 0m;

        //    var range = Enumerable.Range(0, 1 + account.transactions.LastOrDefault().transactionDate.Subtract(account.transactions.FirstOrDefault().transactionDate).Days)
        //      .Select(offset => start.AddDays(offset))
        //      .ToArray();

        //    decimal rate;

        //    var withdrawals = account.transactions
        //         .Where(t => t.transactionType == Transaction.WITHDRAWAL).OrderBy(r => r.transactionDate);

        //    for (int i = 0; i < range.Length; i++)
        //    {
        //        balance += account.SumTransactions(t => t.transactionDate.Equals(range[i]) &&
        //        t.transactionType == Transaction.DEPOSIT || t.transactionType == Transaction.WITHDRAWAL);

        //        if(i >= 10 && !withdrawals.Any())
        //        {
        //            rate = account.InterestRates[Account.maxiHighRate];
        //        }
        //        else if(withdrawals.Last().transactionDate.Date - ...)
        //        {

        //        }

        //        var interest = account.CalculateInterest(balance, rate);
        //        testTime.SetDateTime(range[i]);
        //        account.Deposit(interest, Transaction.INTEREST);
        //        balance += interest;
        //    }

        //    Assert.AreEqual(3502.6588, Math.Round(balance, 4));
        //    DateProvider.ResetInstance();
        //}
    }
}
