﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void TestTransaction()
        {
            Transaction t = new Transaction(5, Transaction.DEPOSIT);
            Assert.AreEqual(true, t is Transaction);
        }
    }
}
